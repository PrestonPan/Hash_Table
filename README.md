# Hash Table
A hash table implementation that does not require the standard
java library.

## introduction
this hash table implementation is in java, and it utilizes
a linked list to handle collisions (separate chaining). This
implementation uses the built in hash function that java 
provides to hash any non-primitive object. It is completely
all purpose in that it is generically typed. 

## building
Build and run with `make` and `make run` respectively. For now,
`BinarySearchTree.java` isn't ready yet so you will have to remove
the file to compile and run.
