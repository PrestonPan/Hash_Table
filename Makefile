# JFLAGS = 
JC = javac
OPT = -O3
#OPT = -g
WARN = -Wall

all:
	$(JC) -d out src/**/*.java

clean:
	rm -rf out

run:
	java -cp out/ hashtable.Main
