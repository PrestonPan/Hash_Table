package hashtable;
public interface Comparable<E> {
    public int compareTo(E obj);
}
