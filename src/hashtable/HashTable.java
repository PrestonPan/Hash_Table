package hashtable;

public class HashTable<K, V> {
    
    private static final int MAX_SIZE = 10000;
    private ArrayList<Bucket> buckets = new ArrayList<>();
    private int size;
    
    public HashTable() {
	for (int i = 0; i < this.MAX_SIZE; i ++) {
	    buckets.add(new Bucket());
	}
    }

    private class Bucket {
	private SinglyLinkedList<Pair> list = new SinglyLinkedList<>();
	public void add(K key, V value) {
	    int index = this.list.indexOf(new Pair<K, V>(key, value));
	    if (index == -1) {
		Pair pair = new Pair<K, V>(key, value);
		this.list.addNode(this.list.size(), pair);
	    } else {
		Pair pair = this.list.get(index);
		pair.value = value;
	    }
      	}

	public void delete(K key) {
	    int index = this.list.indexOf(new Pair<K, V>(key, null));
	    if (index == -1) throw new IllegalArgumentException("Imagine trying to delete a key that doesn't exist. LOL.");
	    else this.list.delNode(index);
	}

	public V find(K key) {
	    Pair pair = this.list.get(this.list.indexOf(new Pair<K, V>(key, null)));
	    if (pair == null) return null;
	    return (V) pair.value;
	}
    }

    public void add(K key, V value) {
	Bucket bucket = this.buckets.get(this.hash(key));
	bucket.add(key, value);
	this.size ++;
    }

    public void delete(K key) {
	Bucket bucket = this.buckets.get(this.hash(key));
	bucket.delete(key);
	this.size --;
    }

    public V find(K key) {
	Bucket bucket = this.buckets.get(this.hash(key));
	return bucket.find(key);
    }
    
    private int hash(K key) {
	return key.hashCode() % MAX_SIZE;
    }
    
    public int size(){
	return this.size;
    }
}
