package hashtable;

class Main {
    public static void main(String[] args) {
	HashTable<Integer, String> hashtable = new HashTable<>();
	hashtable.add(8, "hello");
	hashtable.add(9, "world");
	System.out.println(hashtable.size());
	System.out.println(hashtable.find(8));
	System.out.println(hashtable.find(9));
	hashtable.delete(8);
	System.out.println(hashtable.size());
    }
}
