package hashtable;

public class ArrayList<E> {
    private static final int INIT_CAPACITY = 8;
    private Object[] data = new Object[INIT_CAPACITY];
    private int size = 0;

    public boolean isEmpty() {
	return size == 0;
    }

    public int size() {
	return size;
    }

    private void resize(int capacity) {
	assert capacity >= this.size;
	Object[] newData = new Object[capacity];
	for (int i = 0; i < this.size; i ++) {
	    newData[i] = data[i];
	}
	data = newData;
    }

    public void add(int index, E item) {
	if (size == data.length) resize(size * 2);
	for (int i = index + 1; i < size; i ++) {
	    data[i] = data[i - 1];
	}
	data[index] = item;
	this.size ++;
    }
    
    public void add(E item) {
	if (size == data.length) resize(size * 2);
	data[size++] = item;
    }

    public E get(int index) {
	return (E) data[index];
    }

    public void remove(int index) {
	assert index < size;
	for (int i = index; i < size - 1; i ++) {
	    data[i] = data[i + 1];
	}
    }
    
}
