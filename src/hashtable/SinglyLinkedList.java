package hashtable;

public class SinglyLinkedList<E> {
    private Node head;
    private int size = 0;
    
    private class Node {
	E val;
	Node next;
	public Node(E val, Node next) {
	    this.val = val;
	    this.next = next;
	}
    }

    private Node getNode(int index) {
	if (this.size <= index || index < 0) return null;
	Node node = head;
	for (int i = 0; i < index; i ++){
	    node = node.next;
	}
	return node;
    }

    public E get(int index) {
	if (this.getNode(index) == null) return null;
	return this.getNode(index).val;
    }

    public void addNode(int index, E val) {
	if (this.size < index || index < 0) return;
	if (index == 0) {
	    Node next = this.head;
	    this.head = new Node(val, next);
	} else {
	    Node prev = getNode(index - 1);
	    Node next = prev.next;
	    prev.next = new Node(val, next);
	}
	this.size ++;
    }
    
    public void delNode(int index) {
	if (this.size <= index || index < 0) return;
	if (index == 0) {
	    this.head = this.head.next;
	} else {
	    Node prev = getNode(index - 1);
	    prev.next = prev.next.next;
	}
	this.size --;
    }

    public int indexOf(E val) {
	Node node = this.head;
	int i = 0;
	while (node != null) {
	    if (node.val.equals(val)) {
		return i;
	    }
	    node = node.next;
	    i ++;
	}
	return -1;
    }
    public int size() {
	return this.size;
    }
}
