package hashtable;

public class Pair <K, V> extends Comparable<Pair>{
    K key;
    V value;
    
    public Pair(K key, V value) {
	this.key = key;
	this.value = value;
    }

    @Override
    public final int hashCode() {
	return this.key.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
	
	// If o and this class both point to the same object
	if (o == this) {
	    return true;
	}
	
	// checks if o is null or o is not an instance of a Pair
	if (!(o instanceof Pair)) {
	    return false;
	}

	Pair p = (Pair) o;
	
	return this.key.equals(p.key);
    }
    // implements compareTo from Comparable
    public int compareTo(Pair pair) {
	return this.key.compareTo(pair.key);
    }
}
