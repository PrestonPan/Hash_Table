package hashtable;

public class BinarySearchTree<E extends Comparable<E>> {
    private Node root;
    private int size = 0;
    
    private class Node{
	E val;
	Node left;
	Node right;
	
	public Node(E val) {
	    this.val = val;
	}
    }

    private Node getNode(E val, Node cur) {
	if (cur == null) return null;
	if (cur.val.equals(val)) {
	    return cur;
	}
	if (cur.val.compareTo(val) < 0) {
	    return getNode(val, cur.left);
	}
	return getNode(val, cur.right);
    }
    
    private Node getNode(E val) {
	return getNode(val, root);
    }

    public boolean checkValueExists(E val) {
	return getNode(val) == null;
    }

    private Node addNode(E val, Node cur) {
	if (cur == null) return new Node(val);
	
	if (cur.val.compareTo(val) < 0)
	    cur.left = addNode(val, cur.left);
	else cur.right = addNode(val, cur.right);
	return cur;
    }
    
    public void addNode(E val) {
	if (getNode(val) != null){
	    throw new IllegalArgumentException("Imagine trying to enter a value that exists. LOL.");
	}
	this.root = addNode(val, this.root);
    }

    private Node delNode(E val, Node cur) {
	if (cur == null) return null;
	if (cur.val.compareTo(val) < 0) 
	    cur.left = delNode(val, cur.left);
	else if (cur.val.compareTo(val) > 0) 
	    cur.right = delNode(val, cur.right);
	else {
	    if (cur.left == null) return cur.right;
	    if (cur.right == null) return cur.left;
	    Node leftMax = cur.left;
	    while (leftMax.right != null) {
		leftMax = leftMax.right;
	    }
	    cur.val = leftMax.val;
	    cur.left = delNode(leftMax.val, cur.left);
	}
	return cur;
    }
    
    public void delNode(E val) {
	if (getNode(val) == null)
	    throw new IllegalArgumentException("> trying to delete something that doesn't exist. Lmao.");
	this.root = delNode(val, this.root);
    }
    
    int size() {
	return this.size;
    }
}
